package com.example.infracentre;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.SubMenu;

public class MainActivity extends SherlockActivity {

	Intent intent;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        GridView gridview = (GridView)findViewById(R.id.gridView1);
        gridview.setAdapter(new ImageAdapter(this));
        gridview.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	            switch(position){
	            case 0:
		            Intent intent = new Intent(MainActivity.this, CourseActivity.class);
		            startActivity(intent);
		            break;
	            case 1:
	            Intent intent0 = new Intent(MainActivity.this, LocationsActivity.class);
	            startActivity(intent0);
	            break;
	            case 2:
	            	
				Intent intent1 = new Intent(MainActivity.this, GalleryActivity.class);
				startActivity(intent1);
				break;
		            
	            case 3:
	            	Intent intent2 = new Intent(MainActivity.this, WhyInfraActivity.class);
		            startActivity(intent2);
		            break;
	            case 4:
	            	
	            	Intent intent3 = new Intent(MainActivity.this, ContactUsActivity.class);
		            startActivity(intent3);
		            break;
	            case 5:
	            	Intent intent4 = new Intent(MainActivity.this, AboutUsActivity.class);
		            startActivity(intent4);
		            break;
		        
	            }
	        	
	        }

			
	     }
	  );
    }

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
	
		 SubMenu sub = menu.addSubMenu("More");
		 sub.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		 sub.getItem().setIcon(android.R.drawable.ic_menu_more);

		    getSupportMenuInflater().inflate(R.menu.menu, sub);
		return super.onCreateOptionsMenu(menu);
		
	}

	@Override
	public boolean onMenuItemSelected(int featureId,com.actionbarsherlock.view.MenuItem item) {
		Dialog d;
		switch (item.getItemId()) {
		case R.id.MenuApplyNow:
			Uri uri = Uri.parse("smsto:+923132905165");
			intent = new Intent(Intent.ACTION_SENDTO, uri);
			 intent.putExtra("sms_body", "Hammad hussain\nAndroid"); 
	         startActivity(intent);
			break;
		case R.id.MenuShare:
			intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/*");
			intent.putExtra(Intent.EXTRA_TEXT, "It is a fabulous app");
			startActivity(Intent.createChooser(intent, "Share Using"));
			break;
		case R.id.Menulike:
			
			break;
		case R.id.Menurate:
			
			break;
		case R.id.MenuHelp:
			d = new Dialog(MainActivity.this);
			d.setContentView(R.layout.activity_why_infra);
			d.setTitle("INFRA PROFESSIONAL INSTITUTE");
			d.setCancelable(true);
			d.show();
			break;
		case R.id.MenuAbout:
			d = new Dialog(MainActivity.this);
			d.setContentView(R.layout.activity_about_us);
			d.setTitle("Help.");
			d.setCancelable(true);
			d.show();
			break;
		}
		
		return super.onMenuItemSelected(featureId, item);
	}
	 
    
    
    
}
