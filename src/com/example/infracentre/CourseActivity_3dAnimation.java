package com.example.infracentre;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.widget.ZoomControls;

public class CourseActivity_3dAnimation extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course_activity_3d_animation);
	
		WebView webView = (WebView) findViewById(R.id.webview);
		try{
		webView.loadUrl("file:///android_asset/animation.html");
		}catch(Exception ex){
			ex.printStackTrace();
		}
		webView.getSettings().getJavaScriptEnabled();
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setDefaultZoom(ZoomDensity.MEDIUM);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

}
